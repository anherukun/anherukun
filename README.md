<!--
**anherukun/anherukun** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

# Hi, I'm アンヘルーくん (Angel in Japanese 🇯🇵) 

- I'm currently working in government corporation in Mexico 🇲🇽.
- I'm learning frontend and .NET backend
- I like .NET Technologies, JavaScript and more.
- I would like to learn Japanese

## Tecnhologies
<p class="flex-center-spaced" align="center" style="display: flex; justify-content: space-evenly;">
<img src="https://static-00.iconduck.com/assets.00/c-sharp-c-icon-456x512-9sej0lrz.png" alt="c#" height="40" style="vertical-align:top; margin:4px">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/.NET_Core_Logo.svg/2048px-.NET_Core_Logo.svg.png" alt="dotnet" height="40" style="vertical-align:top; margin:4px">
<img src="http://www.manualweb.net/img/logos/java.png" alt="Java" height="40" style="vertical-align:top; margin:4px">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png" alt="Javascript" height="40" style="vertical-align:top; margin:4px">
<img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" alt="VS Code" height="40" style="vertical-align:top; margin:4px">
<img src="https://jartigag.xyz/assets/images/posts/git.png" alt="Git" height="40" style="vertical-align:top; margin:4px">
</p>

## :trophy: My Github Stats

[![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=anherukun&theme=gotham&show_icons=true)](https://github.com/anuraghazra/github-readme-stats)


